# -*- coding: utf-8 -*-

from Products.CMFCore.utils import getToolByName
from plone import api


def get_data(context, request):
    """
    """
    data = []
    parent_UID = request.get("parent_UID", None)
    depth = request.get("depth", None)
    all_portal_contents = request.get("all_portal_contents", False)
    path = {}
    portal = api.portal.get()

    if parent_UID:
        parent = api.content.get(UID=parent_UID)
        if parent:
            path["query"] = "/".join(parent.getPhysicalPath())
        else:
            return data
    else:
        if not all_portal_contents:
            path["depth"] = 1
        path["query"] = "/".join(portal.getPhysicalPath())
    if depth:
        try:
            depth = int(depth)
        except:
            depth = 1
        path["depth"] = depth

    request.set("path", path)
    brains = context.queryCatalog(REQUEST=request)
    return get_regular_data(context, brains)


def get_regular_data(context, brains):
    """
    """
    data = []
    for brain in brains:
        # author, data/hora, titulo, description, conteudo (imagem, arquivo, link)
        data_dict = get_brain_data(context, brain)
        if data_dict:
            data.append(data_dict)
    return data


def get_brain_data(context, brain):
    """
    """
    data_dict = {}
    user = api.user.get(username=brain.Creator)
    if user:
        data_dict["author"] = user.getUserName()
        data_dict["created"] = brain.created.asdatetime().strftime("%d/%m/%Y %H:%M")
        data_dict["modified"] = brain.modified.asdatetime().strftime("%d/%m/%Y %H:%M")
        data_dict["UID"] = brain.UID
        data_dict["title"] = brain.Title
        data_dict["portal_type"] = brain.portal_type
        data_dict["description"] = brain.Description
        data_dict["url"] = brain.getURL()
        comments = get_comments(context, brain.getPath())
        if comments:
            data_dict["comments"] = comments
    return data_dict


def get_comments(context, parent_path):
    """
    """
    data = []
    portal_catalog = getToolByName(context, "portal_catalog")
    query = {}
    query["path"] = {"query": parent_path, "depth": 2}
    query["portal_type"] = "Discussion Item"
    comments = portal_catalog(query)
    for brain in comments:
        user = api.user.get(username=brain.Creator)
        if user:
            data_dict = {}
            data_dict["author"] = user.getUserName()
            data_dict["created"] = brain.created.asdatetime().strftime("%d/%m/%Y %H:%M")
            data_dict["UID"] = brain.UID
            data_dict["description"] = brain.Description
            data.append(data_dict)
    return data
