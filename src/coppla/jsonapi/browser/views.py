# -*- coding: utf-8 -*-

import json
from Products.Five.browser import BrowserView
from coppla.jsonapi.utils import get_data
from coppla.jsonapi.utils import get_comments
from plone import api


class JsonView(BrowserView):
    """
        Return json consult
    """

    def __call__(self):
        data = get_data(self.context, self.request)
        return json.dumps(data)


class JsonObjectView(BrowserView):
    """
        Return all content object
    """

    def __call__(self):
        """
        """
        UID = self.request.get("UID", None)
        brain = api.content.find(UID=UID)
        data = {}
        try:
            if brain:
                obj = brain[0].getObject()
                schema = obj.schema
                for field in schema.fields():
                    field_name = field.getName()
                    if field_name in [
                        'effectiveDate',
                        'expirationDate',
                        'excludeFromNav',
                        'allowDiscussion',
                        'location',
                    ]:
                        continue
                    field_data = field.getRaw(obj)
                    if field_name in ['creation_date', 'modification_date']:
                        field_data = field_data.asdatetime().strftime("%d/%m/%Y %H:%M")
                    comments = get_comments(self.context, brain[0].getPath())
                    if comments:
                        data["comments"] = comments
                    data["UID"] = UID
                    data[field_name] = field_data
        except:
            data = False
        return json.dumps(data)
