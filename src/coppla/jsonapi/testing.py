# -*- coding: utf-8 -*-
# from plone.app.robotframework.testing import REMOTE_LIBRARY_BUNDLE_FIXTURE
from plone.app.testing import applyProfile
from plone.app.testing import FunctionalTesting
from plone.app.testing import IntegrationTesting
from plone.app.testing import PLONE_FIXTURE
from plone.app.testing import PloneSandboxLayer
from plone.testing import z2

import coppla.jsonapi


class CopplaJsonapiLayer(PloneSandboxLayer):

    defaultBases = (PLONE_FIXTURE,)

    def setUpZope(self, app, configurationContext):
        self.loadZCML(package=coppla.jsonapi)

    def setUpPloneSite(self, portal):
        applyProfile(portal, 'coppla.jsonapi:default')


COPPLA_JSONAPI_FIXTURE = CopplaJsonapiLayer()


COPPLA_JSONAPI_INTEGRATION_TESTING = IntegrationTesting(
    bases=(COPPLA_JSONAPI_FIXTURE,),
    name='CopplaJsonapiLayer:IntegrationTesting'
)


#COPPLA_JSONAPI_FUNCTIONAL_TESTING = FunctionalTesting(
#    bases=(COPPLA_JSONAPI_FIXTURE,),
#    name='CopplaJsonapiLayer:FunctionalTesting'
#)


#COPPLA_JSONAPI_ACCEPTANCE_TESTING = FunctionalTesting(
#    bases=(
#        COPPLA_JSONAPI_FIXTURE,
#        REMOTE_LIBRARY_BUNDLE_FIXTURE,
#        z2.ZSERVER_FIXTURE
#    ),
#    name='CopplaJsonapiLayer:AcceptanceTesting'
#)
