# -*- coding: utf-8 -*-
"""Setup tests for this package."""
from coppla.jsonapi.testing import COPPLA_JSONAPI_INTEGRATION_TESTING  # noqa
from plone import api

import unittest


class TestSetup(unittest.TestCase):
    """Test that coppla.jsonapi is properly installed."""

    layer = COPPLA_JSONAPI_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')

    def test_product_installed(self):
        """Test if coppla.jsonapi is installed."""
        self.assertTrue(self.installer.isProductInstalled(
            'coppla.jsonapi'))

    def test_browserlayer(self):
        """Test that ICopplaJsonapiLayer is registered."""
        from coppla.jsonapi.interfaces import (
            ICopplaJsonapiLayer)
        from plone.browserlayer import utils
        self.assertIn(ICopplaJsonapiLayer, utils.registered_layers())


class TestUninstall(unittest.TestCase):

    layer = COPPLA_JSONAPI_INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')
        self.installer.uninstallProducts(['coppla.jsonapi'])

    def test_product_uninstalled(self):
        """Test if coppla.jsonapi is cleanly uninstalled."""
        self.assertFalse(self.installer.isProductInstalled(
            'coppla.jsonapi'))
