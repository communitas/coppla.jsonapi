# -*- coding: utf-8 -*-
import json
from plone.app.testing import TEST_USER_ID
from plone.app.testing import setRoles
from plone import api

from coppla.jsonapi.testing import COPPLA_JSONAPI_INTEGRATION_TESTING

import unittest2 as unittest


def test_value(key, value, listdata):
    for data in listdata:
        if data.get(key) == value:
            return True
    return False


class ViewsTest(unittest.TestCase):

    layer = COPPLA_JSONAPI_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        self.request = self.layer['request']
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        self.installer = api.portal.get_tool('portal_quickinstaller')
        self.folder1 = api.content.create(
            type='Folder',
            title='Folder1',
            container=self.portal,
        )
        api.content.create(
            type='Document',
            title='Page1',
            container=self.folder1,
        )
        api.content.create(
            type='Link',
            title='Link1',
            container=self.folder1,
        )
        api.content.create(
            type='Folder',
            title='Folder11',
            container=self.folder1,
        )
        self.folder2 = api.content.create(
            type='Folder',
            title='Folder2',
            container=self.portal,
        )
        api.content.create(
            type='Document',
            title='Page2',
            container=self.folder2,
        )
        api.content.create(
            type='Document',
            title='Page3',
            container=self.folder2,
        )

    def test_view_default(self):
        """
            Test return only depth 1 the portal
        """
        view = api.content.get_view(
            name='json-view',
            context=self.portal,
            request=self.request,
        )
        jsondata = view.aq_base()
        jsondata = json.loads(jsondata)
        brains = api.content.find(context=self.portal, depth=1)
        self.assertEqual(len(jsondata), len(brains))
        for i in range(len(brains)):
            self.assertTrue(test_value('title', brains[i].Title, jsondata))
            self.assertTrue(test_value('portal_type', brains[i].portal_type, jsondata))
            self.assertTrue(test_value('url', brains[i].getURL(), jsondata))

    def test_view_all(self):
        """
            Test return all contents the portal
        """
        self.request.set('all_portal_contents', True)
        view = api.content.get_view(
            name='json-view',
            context=self.portal,
            request=self.request,
        )
        jsondata = view.aq_base()
        jsondata = json.loads(jsondata)
        brains = api.content.find(context=self.portal)
        self.assertEqual(len(jsondata), len(brains))
        for i in range(len(brains)):
            self.assertTrue(test_value('title', brains[i].Title, jsondata))
            self.assertTrue(test_value('portal_type', brains[i].portal_type, jsondata))
            self.assertTrue(test_value('url', brains[i].getURL(), jsondata))

    def test_view_uid(self):
        """
            Test return parent_UID and sons
        """
        self.request.set('parent_UID', self.folder1.UID())
        view = api.content.get_view(
            name='json-view',
            context=self.portal,
            request=self.request,
        )
        jsondata = view.aq_base()
        jsondata = json.loads(jsondata)
        brains = api.content.find(context=self.folder1)
        self.assertEqual(len(jsondata), len(brains))
        for i in range(len(brains)):
            self.assertTrue(test_value('title', brains[i].Title, jsondata))
            self.assertTrue(test_value('portal_type', brains[i].portal_type, jsondata))
            self.assertTrue(test_value('url', brains[i].getURL(), jsondata))

    def test_view_portal_types(self):
        """
            Test return all contents with portal_type filter
        """
        self.request.set('all_portal_contents', True)
        self.request.set('portal_type', 'Document')
        view = api.content.get_view(
            name='json-view',
            context=self.portal,
            request=self.request,
        )
        jsondata = view.aq_base()
        jsondata = json.loads(jsondata)
        brains = api.content.find(context=self.portal, portal_type='Document')
        self.assertEqual(len(jsondata), len(brains))
        for i in range(len(brains)):
            self.assertTrue(test_value('title', brains[i].Title, jsondata))
            self.assertTrue(test_value('portal_type', brains[i].portal_type, jsondata))
            self.assertTrue(test_value('url', brains[i].getURL(), jsondata))

    def test_view_uid_portal_types(self):
        """
            Test return parent_UID contents with portal_type filter
        """
        self.request.set('parent_UID', self.folder2.UID())
        self.request.set('portal_type', 'Document')
        view = api.content.get_view(
            name='json-view',
            context=self.portal,
            request=self.request,
        )
        jsondata = view.aq_base()
        jsondata = json.loads(jsondata)
        brains = api.content.find(context=self.folder2, portal_type='Document')
        self.assertEqual(len(jsondata), len(brains))
        for i in range(len(brains)):
            self.assertTrue(test_value('title', brains[i].Title, jsondata))
            self.assertTrue(test_value('portal_type', brains[i].portal_type, jsondata))
            self.assertTrue(test_value('url', brains[i].getURL(), jsondata))

    def test_view_single_object(self):
        """
            Test return contents single object
        """
        self.request.set('UID', self.folder2.UID())
        view = api.content.get_view(
            name='json-object-view',
            context=self.portal,
            request=self.request,
        )
        jsondata = view.aq_base()
        jsondata = json.loads(jsondata)
        brain = api.content.get(UID=self.folder2.UID())
        self.assertEqual(brain.Title(), jsondata.get('title'))
        self.assertEqual(brain.UID(), jsondata.get('UID'))
        # test False
        self.request.set('UID', "00000000000")
        view = api.content.get_view(
            name='json-object-view',
            context=self.portal,
            request=self.request,
        )
        jsondata = view.aq_base()
        jsondata = json.loads(jsondata)
        self.assertFalse(jsondata)
