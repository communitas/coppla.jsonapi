==============================================================================
coppla.jsonapi
==============================================================================

The access to the portal content in JSON format is made through a view accessible from the portal root.

Installation
------------

Access src folder and clone this repository.


Install coppla.jsonapi by adding it to your buildout::

    [buildout]

    ...

    eggs =
        coppla.jsonapi

    develop =
        src/coppla.jsonapi


and then running ``bin/buildout``


Documentation
-------------

In the URL of the view is appended the portal_catalog query parameters. The query is made using the “queryCatalog” method that build the query using the parameters in the request. The query is made by the context, will soon return just what the user is allowed to access.

URL = http://URL_PORTAL/@@json-view

- After the processing of the URL access, only the data of the portal's children will be returned, Depth = 1::

    [{"UID": "65e311d52629495ba6f6b694fe6752c6", "title": "Welcome to Plone", "url": "http://localhost:8080/Dio/front-page", "portal_type": "Document", "created": "25/11/2015 10:49", "modified": "25/11/2015 10:49", "author": "admin", "description": "Congratulations! You have successfully installed Plone."}, {"UID": "0ef81de2c76b44fa8c1e85dccd0c495a", "title": "A", "url": "http://localhost:8080/Dio/a", "portal_type": "Folder", "created": "13/11/2015 15:44", "modified": "13/11/2015 15:46", "author": "admin", "description": "teste"}]


- Access the ``URL/@@json-view?all_portal_contents=true``. If "all_portal_contents=true", the server will return all of the portal data.. Depth=-1.

- Access the ``URL/@@json-view?parent_UID=UID``. The parameter parent_UID is the UID of a folder that you want to get the data, the server will return the contents of the folder.

- Access the ``URL/@@json-view?parent_UID=UID&portal_type=Document``. The parameter parent_UID is the UID of a folder that you want to get the data plus the parameter 'portal_type' Document, the server will return the contents of the folder that is of Document type. The parameter 'portal_type' can be used in the home portal.

Listing all attributes of an OBJ:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

URL = http://URL_PORTAL/@@json-object-view?UID=UID_OBJ

The JSON data returned by the query, in the case of an Archetype object, returns all of its attributes. Attention with this view, because it wakes up the object::

    author: Creator's name (String)
    created: Create Date “dd/mm/aaaa hh:mm” (String)
    modified: Modified Date “dd/mm/aaaa hh:mm” (String)
    UID: UID the content (String)
    title: Title (String)
    description: Description (String)
    url: URL (String)
    comments: Comments list. Each list item has the keys: author, created, UID e description
    depth: depth == -1 

Parameter:
^^^^^^^^^

- Filtering only one type::

    ?portal_type=Document

- List all the portal folders::

    ?portal_type=Folder

- Filtering a type list::

    ?portal_type:list=Document&portal_type:list=Link

- Filtering a range of dates (creation date)::

    ?created.query:record:list:date=2014/07/17
    &created.query:record:list:date=2014/07/19
    &created.range:record=min:max

- Set the starting point of the search::

    ?parent_UID=UID

- Data from a specific user::

    ?Creator=id_creator

- Sort by a particular field::

    Creation date: ?sort_on=created
    Title: ?sort_on=sortable_title

- Ascending or descending order::

    Ascending: ?sort_order=ascending
    Descending: ?sort_order=reverse

Contribute
----------

- Issue Tracker: communitas@communitas.org.br
- Source Code: https://bitbucket.org/communitas/coppla.jsonapi

License
-------

The project is licensed under the GPLv2.